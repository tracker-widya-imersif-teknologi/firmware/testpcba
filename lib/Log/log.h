class Log{
    private:
    public:
    
    Log(uint32_t baudrate){
        delay(3000);
        Serial.begin(baudrate); // Set console baud rate
    }
    void info(String tag, String data, bool alenia){
        String message = "[ " + String(millis()) + " ]";
        message += "[ INFO ]";
        message += "[ " + tag + " ]";
        message += " " + data;
        
        if(alenia)Serial.println(message);
        else Serial.print(message);
    }

    void test(String tag, String data, bool alenia){
        String message = "[ " + String(millis()) + " ]";
        message += "[ TEST ]";
        message += "[ " + tag + " ]";
        message += " " + data;

        if(alenia)Serial.println(message);
        else Serial.print(message);
    }
    
    void warning(String tag, String data, bool alenia){
        String message = "[ " + String(millis()) + " ]";
        message += "[ WARNING ]";
        message += "[ " + tag + " ]";
        message += " " + data;

        if(alenia) Serial.println(message);
        else Serial.print(message);
    }
};

Log trackerlog(74880);
