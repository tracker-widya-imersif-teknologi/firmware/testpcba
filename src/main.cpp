#include "init.h"

void setup() {
  versionInfo(); // DONE Give Version
  ledtracker.runningLed(10);
  ledtracker.allDown();
  delay(2000);
  /* Starting */
  Serial.println("-------------------------- Test Starting --------------------------");
  inSensor.init();
  sim.init();
  gps.init();
}
void loop() {
  done();
}