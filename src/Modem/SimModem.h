#define SerialAT  Serial1

// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb

#include "TinyGsmClient.h"
TinyGsm modem_sim(SerialAT);

class SimModem{
    private:

    /* cellular component */
    String ccid = "";
    String imei = "";
    String info = "";
    String tagTesting = "Sim Testing";
    
    String eraseSpaceAre(String res){
        res.replace("\n","");
        res.replace("\r","");
        
        return res;
    }

    /* AT Software */
    String cmd_at(String atcommand, int time_out){
        atcommand.toUpperCase();
        SerialAT.println(atcommand);
        SerialAT.setTimeout(time_out);
        String respond = SerialAT.readString();
        return respond;
    }

    bool at(){
        SerialAT.println("AT");
        SerialAT.setTimeout(200);
        String respondSerialAT = SerialAT.readString();
        return respondSerialAT.indexOf("OK") >= 0;
    }
    
    String getccid(){
        String respond = cmd_at("AT+CCID", 500);
        if(respond.indexOf("OK") >= 0){
            respond = respond.substring(respond.indexOf("\n"), respond.lastIndexOf("OK"));
            respond = eraseSpaceAre(respond);
        }
        else respond = "";

        // Serial.println("respond ccid : " + respond);
        return respond;
    }

    uint8_t get_signal(){
        uint8_t signal = 0;
        String cmd = "AT+CSQ";
        String cmdSignal = cmd_at(cmd, 500);
        cmdSignal.replace(cmd, "");
        cmdSignal.replace("OK", "");
        cmdSignal.replace("+CSQ:", "");
        cmdSignal.replace("\n", "");
        cmdSignal.replace("\r", "");
        cmdSignal.replace(" ", "");
        int indexComma = cmdSignal.indexOf(",");
        if(indexComma >= 0){
            cmdSignal = cmdSignal.substring(0, indexComma);
        }
        else return 0;
        if(util.isValidNumber(cmdSignal) == false){
            return 0;
        }

        signal = atol(cmdSignal.c_str());
        if(signal == 0){
            signal = 0;
        }
        else if(signal == 1){
            signal = 1;
        }
        else if(signal > 1 or signal < 32){
            signal = map(signal, 2, 31, 2, 100);
        }
        else signal = 0;
        return signal;
    }

    void setupBaudRate(uint32_t baudrate){
        static const int RXPin = 27, TXPin = 26;
        SerialAT.begin(baudrate, SERIAL_8N1, TXPin, RXPin);
    }

    void deviceinfo(){
        while (true){
            this->ccid = getccid(); 
            this->imei = modem_sim.getIMEI();
            this->info = modem_sim.getModemInfo();
            if(
                String(this->info).length() > 0 and
                String(this->ccid).length() > 10 and
                String(this->imei).length() > 0
            ){
                trackerlog.info(this->tagTesting, "Modem Info\t" + this->info, 1);
                trackerlog.info(this->tagTesting , "CCID\t" + this->ccid, 1);
                trackerlog.info(this->tagTesting, "IMEI\t" + this->imei, 1);
                break;
            }
            ledtracker.warningBlink(500, 2);
        }
    }

    void trigger_sim(){
        trackerlog.info("Trigger Sim", "*** ### *** ### *** ###", 1);
        pinMode(22, OUTPUT);
        digitalWrite(22, LOW);
        delay(1000);
        digitalWrite(22, HIGH);
        delay(1000);
        digitalWrite(22, LOW);
    }

    uint32_t scanBaudRate(){
        uint32_t rate_result = 0;
        uint32_t rates[] = {115200, 57600, 38400, 9600};

        for (int i = 0; i < sizeof(rates) / sizeof(rates[0]); i++) {
            trackerlog.info(this->tagTesting, "Make sure baudrate @" + String(rates[i]), 1);
            setupBaudRate(rates[i]);
            unsigned int counterAT = 0;
            for (unsigned int i = 0; i < 15; i++) {
                ledtracker.succedBlink(500, 2);
                if (at() == true) {
                    trackerlog.info(this->tagTesting, "counter at " + String(counterAT) + " times", 1);
                    counterAT++;
                }
                else counterAT = 0;
            }
            if (counterAT > 3) {
                trackerlog.info(this->tagTesting, "counter at over 3 times, it's succed", 1);
                rate_result = rates[i];
                break;
            }
            else{
                ledtracker.warningBlink(500, 2);
            }
        }

        return rate_result;
    }

    void repeatGetSignal(){
        for(int i = 0; i < 5; i++){
            ledtracker.succedBlink(500, 2);
            trackerlog.info(this->tagTesting, "signal " + String(get_signal()) + " %", 1);
            delay(500);
        }
    }

    public:
    /* Flow */
    void init(){
        ledtracker.stepBlink(1000, 2);
        unsigned int counterWarning = 0;
        // trigger_sim();
        while (true){
            uint32_t rate_result = scanBaudRate();
            trackerlog.info(this->tagTesting, "baudrate @" + String(rate_result), 1);
            if(rate_result > 100){
                setupBaudRate(rate_result);
                trackerlog.info(this->tagTesting, "it's found baudrate @" + String(rate_result), 1);
                this->deviceinfo();
                this->repeatGetSignal();
                break;
            }
            else {
                trigger_sim();
                counterWarning++;
            }

            if(counterWarning > 3){
                ledtracker.warningBlink(500, 2);
            }
        }
        ledtracker.allBlink(500, 2);
    }
};

SimModem sim;
