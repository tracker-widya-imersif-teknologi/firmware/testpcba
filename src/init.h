#include <Arduino.h>

#include "log.h"
#include "Util/util.h"
#include "IndicatorLed/indicatorled.h"

#include "InputSensor/inputsensor.h"
#include "Modem/SimModem.h"
#include "Gps/gps.h"

void versionInfo(){
    delay(2000);
    Serial.println("----------- Versioning -----------");
    trackerlog.info("Build", __TIMESTAMP__, 1);
    trackerlog.info("Flash", String(ESP.getFlashChipSize() / 1024) + " KiloByte", 1);
    trackerlog.info("SDK", String(ESP.getSdkVersion()), 1);
    trackerlog.info("Chip Rev", String(ESP.getChipRevision()), 1);
    trackerlog.info("Free Memory", String(ESP.getFreeHeap()) + " KiloByte", 1);
    Serial.println("----------------------------------");
    delay(1000);
}

void done(){
  unsigned long tmDone = millis();
  while(true){
    unsigned tmDoneAgo = millis() - tmDone;
    Serial.print(String("\r") + "------- Testing is done " + tmDoneAgo + "\tmillis ago -------");
    delay(100);
    ledtracker.allBlink(100, 1);
  }
}