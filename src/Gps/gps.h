#include "HardwareSerial.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_Ublox_GPS

HardwareSerial mySerial(2);
SFE_UBLOX_GPS myGPS;

class Gps{
private:
String tagTesting = "Gps Testing";
public:
    void init(){
        ledtracker.stepBlink(1000, 3);
        uint32_t rates[] = {38400, 115200, 9600, 57600};
        bool isDone = false;
        while (isDone == false){
            for (int i = 0; i < sizeof(rates) / sizeof(rates[0]); i++) {
                mySerial.begin(rates[i]);
                trackerlog.info(this->tagTesting, "Make sure baudrate @" + String(rates[i]), 1);
                if (myGPS.begin(mySerial) == true){
                    // Serial.println("GPS: connected at 9600 baud, switching to 38400");
                    myGPS.setSerialRate(38400);
                    String message = "Gps Connected @" + String(rates[i]) + " but swithced to 38400";
                    trackerlog.info(this->tagTesting, message, 1);

                    isDone = true;
                    break;
                }
                if(i == sizeof(rates) / sizeof(rates[0])){
                    i = 0;
                    ledtracker.warningBlink(500, 3);
                }
            }
        }
        ledtracker.allBlink(500, 3);
    }
};

Gps gps;