class InputSensor{
    private:
        const unsigned int OPERATING_STATUS = 32;
        const unsigned int INPUT_SENSOR_1 = 34;
        const unsigned int INPUT_SENSOR_2 = 35;

        bool get(unsigned int pin){
            bool result = false;
            if(analogRead(pin) > 1000){
                result = true;
            }
            return result;
        }

    public:
        InputSensor(){
            pinMode(this->OPERATING_STATUS, INPUT);
            pinMode(this->INPUT_SENSOR_1, INPUT);
            pinMode(this->INPUT_SENSOR_2, INPUT);
        }
        void init(){
            ledtracker.stepBlink(1000, 1);
            unsigned int counter = 0;
            while (true){
                bool state_os   = get(this->OPERATING_STATUS);
                bool state_in_1 = get(this->INPUT_SENSOR_1);
                bool state_in_2 = get(this->INPUT_SENSOR_2);
                if(state_os > 0 && state_in_1 > 0 && state_in_2 > 0){
                    ledtracker.succedBlink(500, 1);
                    trackerlog.info("Operating Status", "--- done ---", 1);
                    counter++;
                }
                else{
                    ledtracker.warningBlink(500, 1);
                    trackerlog.info("Operating Status", "--- something must installed ---", 1);
                    counter = 0;
                }

                if(counter > 10){
                    break;
                }
                delay(500);
            }
            ledtracker.allBlink(500, 1);
        }
};

InputSensor inSensor;