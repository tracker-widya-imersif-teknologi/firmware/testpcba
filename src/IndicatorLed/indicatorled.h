class IndicatorLed{
    private:
        const unsigned int red   = 2;
        const unsigned int blue  = 13;
        const unsigned int green = 15;

        void init(){
            pinMode(this->red, OUTPUT);
            pinMode(this->green, OUTPUT);
            pinMode(this->blue, OUTPUT); 
        }
    public:
        IndicatorLed(){
            init();
        }
        void stepBlink(unsigned int time, int count){
            for(int i = 0 ; i < count ; i++){
                digitalWrite(this->blue, HIGH);
                delay(time);
                digitalWrite(this->blue, LOW);
                delay(time);
            }
        }
        void warningBlink(unsigned int time, int count){
            for(int i = 0 ; i < count ; i++){
                digitalWrite(this->red, HIGH);
                delay(time);
                digitalWrite(this->red, LOW);
                delay(time);
            }
        }
        void succedBlink(unsigned int time, int count){
            for(int i = 0 ; i < count ; i++){
                digitalWrite(this->green, HIGH);
                delay(time);
                digitalWrite(this->green, LOW);
                delay(time);
            }
        }
        void allBlink(unsigned int time, int count){
            for(int i = 0 ; i < count ; i++){
                digitalWrite(this->red, HIGH);
                digitalWrite(this->green, HIGH);
                digitalWrite(this->blue, HIGH);
                delay(time);
                digitalWrite(this->red, LOW);
                digitalWrite(this->green, LOW);
                digitalWrite(this->blue, LOW);
                delay(time);
            }
        }

        void allDown(){
            digitalWrite(this->red, LOW);
            digitalWrite(this->green, LOW);
            digitalWrite(this->blue, LOW);
        }

        void runningLed(unsigned int time){
            for(int i = 0; i < 30; i++){
                succedBlink(time, 1);
                warningBlink(time, 1);
                stepBlink(time, 1);
                
                stepBlink(time, 1);
                warningBlink(time, 1);
                succedBlink(time, 1);
            }
        }
};

IndicatorLed ledtracker;
